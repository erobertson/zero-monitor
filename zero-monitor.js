#!/usr/bin node
var http = require('http');
var currentIp = null;
var gpio = require('pi-gpio');

var Lcd = require('lcd'),
	lcd = new Lcd({
		rs: 27,
		e: 22,
		data: [17, 18, 23, 24],
		cols: 16,
		rows: 2
	});

var sensorLib = require('node-dht-sensor');
//refactor
var readingC;
var readingF;
var humidity;

var sensor = {
	initialize: function() {
		return sensorLib.initialize(22, 4);
	},
	read: function(){
		var reading = sensorLib.read();
		readingC = reading.temperature.toFixed(2);
		readingF = (readingC * (9.0 / 5.0) + 32).toFixed(2);
		humidity = reading.humidity.toFixed(2)
		setTimeout(function() {
			sensor.read();
		}, 2500);
	}
};

function printTemperature(){
	lcd.clear(function(err){
		if(err) throw err;
		//log to heroku if we have an ip address (temporary)
	        lcd.setCursor(0, 0);
		lcd.print(readingC + 'C ' + readingF + 'F', function(err){
			if(err) throw err;
			lcd.setCursor(0, 1);
			lcd.print('humidity: ' + humidity + '%', function(err){
				if(err) throw err;
			});
		});
	});
}

var functionSwitch = 0;
lcd.on('ready', function(){
	console.log('LCD ready.');
	lcd.clear(); //start the program with a clear LCD
	//switch between temperature and clock/ip every 10 seconds
	setInterval(function(){
		functionSwitch++;
		if(functionSwitch > 1) functionSwitch = 0;
		if(functionSwitch === 0) printTemperature();
		if(functionSwitch === 1) printTimeAndIp();
	}, 10000);
});

process.on('SIGINT', function(){
	lcd.clear();
	lcd.close();
	process.exit();
});

function printIp(){
	//get list of network interfaces
	var ip = "No IP";
	try{
		var osInt = require('os').networkInterfaces();
        	ip = osInt.wlan0[0].address;
		currentIp = ip;
	}catch (e){
		currentIp = null;
		//error is reported as no IP on the LCD
	}
        lcd.setCursor(0, 1);
        lcd.print(ip, function(err){
                if(err) throw err;
        });
}

function printTimeAndIp(){
	lcd.clear(function(err){
		if(err) throw err;
		lcd.setCursor(0, 0);
		lcd.print(new Date().toString().substring(16,24), function(err){
			if(err) throw err;
			printIp();
		});
	});
}

function logTemperature(readingC, readingF, humidity){
	//eventually the plan is to store these values and log them
	//when network becomes available but for now just only log 
	//when network is available
	if(!currentIp) return;
	var httpOptions = {
		host: "whispering-brook-51660.herokuapp.com",
		path: "/logService/?deviceName=piZero&reportC="
			+readingC+"&reportF="+readingF+
			"&status=Pi%20Check-In:"+readingC+"C%20"+
			readingF+"F%20"+humidity+"percent%20humidity."
	};

	callback = function(response){
		var resp = '';
	
		response.on('data', function(chunk){
			resp += chunk;
		});

		response.on('end', function(){
			console.log('Response complete:');
			console.log(resp);
		});
	}
	
	http.request(httpOptions, callback).end();
}

setInterval(function(){
		logTemperature(readingC, readingF, humidity);
	    }, 1000 * 60 * 1);

if(sensor.initialize()){
	sensor.read();
}else{
	console.warn('Failed to initialize dht22.');
}
